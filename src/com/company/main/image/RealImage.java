package com.company.main.image;

public class RealImage implements Image{
	String name;

	public RealImage(String name) {
		this.name = name;
		loadImage();
	}

	public Image loadImage() {
		System.out.printf("Loading Image %s\n", name);
		return this;
	}

	@Override
	public void display() {
		System.out.printf("Displaying Image %s\n", name);
	}
}
