package com.company;

import com.company.main.proxy.ImageProxy;

public class Main {
	public static void main(String[] args) {
		ImageProxy proxy = new ImageProxy("cat.png");
		proxy.display();
		proxy.display();
	}
}
