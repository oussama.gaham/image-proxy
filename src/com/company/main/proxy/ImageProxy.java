package com.company.main.proxy;

import com.company.main.image.Image;
import com.company.main.image.RealImage;

public class ImageProxy implements Image {
	private final String name;

	public RealImage getImage() {
		return image;
	}

	private RealImage image;

	public ImageProxy(String name) {
		this.name = name;
	}

	public void display(){
		if(image == null)
			image = new RealImage(name);
		image.display();
	}
}
